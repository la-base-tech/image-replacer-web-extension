import browser from 'webextension-polyfill';
import srcset from 'srcset';
import { messageTypes, IMAGES_REPLACEMENTS } from './constants';
import { removeProtocolFromUrl } from './utils';

function getImages() {
  return Array.from(document.getElementsByTagName('img'));
}

function getAbsoluteUrl(relativeUrl) {
  const a = document.createElement('a');
  a.href = relativeUrl;
  const url = a.href;
  return url;
}

function findElementWithUrl(url) {
  const element = getImages().find(image => {
    const imageSrc = removeProtocolFromUrl(image.currentSrc);
    if (imageSrc === url) {
      return true;
    }
    return false;
  });
  return element;
}

// TODO: filter urls (ie: base64 image urls)
function getAllImageUrls(image) {
  const urlsFromSrcSet = srcset.parse(image.srcset).map(({ url }) => getAbsoluteUrl(url));
  const absoluteUrl = getAbsoluteUrl(image.src);
  const urls = [absoluteUrl, ...urlsFromSrcSet];
  return urls;
}

function unhookPortraitByUrl(url) {
  // Find element and replace it
  const el = findElementWithUrl(url);

  // Element not found
  if (!el) {
    return;
  }

  // Get all image urls (src, srcset...) to make sure the urls are registered
  const urls = getAllImageUrls(el);

  // Send to background process the urls
  browser.runtime.sendMessage({
    type: messageTypes.ADD_URLS_FROM_UI,
    urls,
  });

  // TODO: animation & replacement
  const urlReplacement = IMAGES_REPLACEMENTS[0];
  el.src = urlReplacement;
  el.srcset = urlReplacement;
}

browser.runtime.onMessage.addListener(request => {
  switch (request.type) {
    case messageTypes.URL_ADDED_FROM_CONTEXT_MENU:
      unhookPortraitByUrl(request.url);
      break;
    default:
      throw new Error(`unknown message type ${request.type}`);
  }
});
