export function removeProtocolFromUrl(url) {
  return url
    .replace(/^https?:\/\//, '')
    .replace(/^file:\/\//, '')
    .replace(/^\/\//, '');
}
