import browser from 'webextension-polyfill';
import * as api from './lib/api';
import * as storage from './lib/storage';
import { messageTypes, IMAGES_REPLACEMENTS, API_ENDPOINT } from './constants';
import { removeProtocolFromUrl } from './utils';

let urls = [];

async function updateUrls() {
  urls = await api.loadUrls(API_ENDPOINT);
  await storage.saveUrls(urls);
}

async function addUrlsFromUI(newUrls) {
  // Add formatted urls to the in-memory store
  newUrls.forEach(url => {
    const formattedUrl = removeProtocolFromUrl(url);
    urls.push(formattedUrl);
  });

  // Update localstorage
  await storage.saveUrls(urls);

  // Push to the API
  await api.addUrls(API_ENDPOINT, newUrls);
}

async function onAddUrl(url) {
  const formattedUrl = removeProtocolFromUrl(url);
  urls.push(formattedUrl);

  // Send a message to the frontend to unhook the image
  const tabs = await browser.tabs.query({
    currentWindow: true,
    active: true,
  });

  tabs.forEach(tab => {
    browser.tabs.sendMessage(tab.id, {
      type: messageTypes.URL_ADDED_FROM_CONTEXT_MENU,
      url: formattedUrl,
    });
  });

  await storage.saveUrls(urls);
  await api.addUrl(API_ENDPOINT, url);
}

// Load urls from storage
storage.getUrls().then(urlsStored => {
  if (!urlsStored) {
    return;
  }
  urls = urlsStored;
});

// Load urls when the extension is installed.
browser.runtime.onStartup.addListener(() => {
  updateUrls();
});

// Create a context menu to unhook a portrait.
// TODO: add context menu on all elements to support also div, img behing an element...
browser.contextMenus.create({
  id: 'unhook',
  title: 'Décrocher le portrait de Macron',
  contexts: ['image'],
  onclick: ({ srcUrl }) => {
    onAddUrl(srcUrl);
  },
});

const filters = {
  urls: ['<all_urls>'],
};

// Add a hook to rewrite blocked urls.
chrome.webRequest.onHeadersReceived.addListener(
  details => {
    if (details.type !== 'image') {
      return null;
    }
    if (!(urls && urls.length)) {
      return null;
    }
    const url = removeProtocolFromUrl(details.url);

    if (urls.includes(url)) {
      return {
        redirectUrl: IMAGES_REPLACEMENTS[0],
      };
    }
    return null;
  },
  filters,
  ['blocking']
);

// Message from the tabs
browser.runtime.onMessage.addListener(request => {
  switch (request.type) {
    case messageTypes.ADD_URLS_FROM_UI:
      addUrlsFromUI(request.urls);
      break;
    default:
      throw new Error(`request type ${request.type} not supported`);
  }
});
