import browser from 'webextension-polyfill';

export const API_ENDPOINT = '';

export const messageTypes = {
  ADD_URL: 'ADD_URL',
  URL_ADDED_FROM_CONTEXT_MENU: 'URL_ADDED_FROM_CONTEXT_MENU',
  ADD_URLS_FROM_UI: 'ADD_URLS_FROM_UI',
};

export const WHITELIST_DOMAINS = [];

export const IMAGES_REPLACEMENTS = [browser.runtime.getURL('images/default.jpg')];
