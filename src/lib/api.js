import uniq from 'lodash.uniq';

export function loadUrls(apiEndpoint) {
  return fetch(`${apiEndpoint}/filters`)
    .then(response => response.text())
    .then(text => {
      const urls = text.split('\n').filter(url => !!url);
      return uniq(urls);
    });
}

// TODO: implement new endpoints
export function addUrl(apiEndpoint, url) {
  return fetch(`${apiEndpoint}?url=${encodeURIComponent(url)}`);
}

// TODO: implement api multi urls post
export async function addUrls(apiEndpoint, urls) {
  for (let i = 0; i < urls.length; i += 1) {
    await addUrl(apiEndpoint, urls[i]);
  }
}
