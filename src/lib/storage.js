import browser from 'webextension-polyfill';

export async function saveUrls(urls) {
  await browser.storage.local.set({ urls });
}

export async function getUrls() {
  const { urls } = await browser.storage.local.get('urls');
  return urls;
}
