# image-replacer-web-extension

Web extension built with the boilerplate [vue-web-extension](https://github.com/Kocal/vue-web-extension).

## dependencies

* yarn

## usage

* Install the dependencies: `yarn`
* Configure the server url in `./src/constants.js`
* Install extension unpackaged in the browser
* Run webpack with the watcher: `yarn run watch:dev`
